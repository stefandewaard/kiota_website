import { AppBrowserModule } from './app/modules/app.browser.module';
import { enableProdMode } from '@angular/core';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

import { environment } from './environments/environment';
import 'hammerjs';


if (environment.production) {
  enableProdMode();
}


platformBrowserDynamic().bootstrapModule(AppBrowserModule)
  .catch(err => console.log(err));

