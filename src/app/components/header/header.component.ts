import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import * as Reducer from './../../reducers';
import { Observable } from 'rxjs';
import { AngularFireStorage } from '@angular/fire/storage';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  $logo: Observable<string>;
  constructor(store: Store<Reducer.iState>, private storage:AngularFireStorage) {
    store.select(Reducer.AppState).map(app => app.company.logo.header).subscribe((logo)=>{
      console.log(logo)
     let $logo = this.storage.storage.ref(logo).getDownloadURL()
     console.log($logo)
    });
  }

  ngOnInit() {
  }

}
