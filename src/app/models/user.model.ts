import { Observable } from "rxjs";
import * as moment from 'moment';

export const READER = 'reader';
export const EDITOR = 'editor';
export const ADMIN = 'admin';
export type Roles = 'reader' | 'editor' | 'admin';
type sex = 'man' | 'woman' | null;
export interface iUser {
  id?: string;
  firstName: string;
  lastName: string;
  sex: sex;
  email: string;
  birthday?: number;
  address: iAddress[];
  phone: string;
  role: Roles;
  newsletter: boolean;
  $image?: Observable<string>;
  created: moment.Moment;
  last_online: moment.Moment;
}

interface iAddress {
  street: string;
  number: number;
  addition?: string | number;
  zipcode: string;
  city: string;
  country: string;
}
