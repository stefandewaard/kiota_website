export interface iTags{
  title?:string;
  description?:string;
  image?:string;
  slug?:string;
}
