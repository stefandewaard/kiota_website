import { Router } from '@angular/router';
import { AngularFirestore } from '@angular/fire/firestore';
import { Injectable } from '@angular/core';
import { Action } from '@ngrx/store';

import * as AuthReducer from './../reducers/auth.reducer';
import { Effect, Actions } from '@ngrx/effects';
import { FireList } from '../../../settings/firebase';
import { iUser } from '../models/user.model';
import { Observable } from 'rxjs';
import { filter } from 'rxjs/operators';

import 'rxjs/add/operator/map'
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/observable/fromPromise';
import { of } from 'rxjs/observable/of';

import { AngularFireAuth } from '@angular/fire/auth';
import * as firebase from 'firebase';
import { catchError } from 'rxjs/operators';



@Injectable()
export class AuthService {
  mail: string;
  password: string;

  constructor(private actions: Actions,
    private readonly auth: AngularFireAuth,
    private readonly store: AngularFirestore,
    private router: Router
  ) {
  }
  @Effect()
  GetUser: Observable<Action> = this.actions.pipe(
    filter(action => action.type === AuthReducer.GET_USER)
  ).map((action: AuthReducer.GetUser) => action)
    .switchMap((payload) => { return Observable.fromPromise(this.user()) })
    .map((user) => {
      if (user !== undefined) {
        return new AuthReducer.Login(user);
      }
      else {
        return new AuthReducer.Logout();
      }
    });

  @Effect()
  OnLogin: Observable<Action> = this.actions.pipe(filter(action => action.type === AuthReducer.ONLOGIN))
    .map((action: AuthReducer.OnLogin) => { return action })
    .switchMap((action) => { return Observable.fromPromise(this.login(action.payload)) })
    .pipe(
      catchError(val => of(val))
    )
    .map((user) => {
      if ('id' in user) {
        this.router.navigate(['']);
        return new AuthReducer.Login(user);
      }
      else {
        return new AuthReducer.Error(user);
      }
    })

  @Effect()
  OnLogout: Observable<Action> = this.actions.pipe(filter(action => action.type === AuthReducer.ONLOGOUT))
    .map((action: AuthReducer.OnLogout) => action)
    .switchMap(() => { return this.auth.auth.signOut() })
    .map(() => {
      // this.router.navigate([''])
      return new AuthReducer.Logout();
    })

  private user(): Promise<iUser> {
    return new Promise((resolve, reject) => {
      this.auth.auth.onAuthStateChanged((auth) => {
        if (auth) {
          this.getUser().then((user: iUser) => {
            resolve(user);
          });
        } else {
          resolve();
        }
      })
    })
  }

  public login(value): Promise<any> {
    return new Promise((resolve, reject) => {
      this.auth.auth.signInWithEmailAndPassword(value.mail, value.password).then((value) => {
        this.getUser().then((user: iUser) => {
          resolve(user);
        })
      }).catch((error) => { reject(error) })
    })
  }

  public loginFacebook() {
    const provider = new firebase.auth.FacebookAuthProvider();
    provider.addScope('');
    provider.addScope('');

    this.auth.auth.signInWithPopup(provider)
    .then((res) => {
      })
    .catch((err) => console.log(err));
  }

  public getUser(): Promise<iUser> {
    return new Promise((resolve) => {
      this.store.collection(FireList.firestore.users).doc(this.auth.auth.currentUser.uid).snapshotChanges().map(doc => {
        const user = doc.payload.data() as iUser;
        user.id = doc.payload.id;
        return ({ ...user })
      }).subscribe(user => {
        resolve(user)
      })
    });
  }
}
