import { Injectable } from '@angular/core';
import { Meta, Title } from '@angular/platform-browser';
import { iTags } from '../models/meta.model';
import { iCompany } from './../reducers/app.reducer';
import { Store } from '@ngrx/store';
import * as Reducer from './../reducers';
import { Router, NavigationEnd } from '@angular/router';
import { baseUrl } from './../../../settings/firebase';

@Injectable({
  providedIn: 'root'
})
export class MetaService {

  company: iCompany;
  tags: iTags;
  url: string;

  constructor(private meta: Meta, private title: Title, store: Store<Reducer.iState>, router: Router) {
    store.select(Reducer.AppState).map(app => app.company).subscribe((company: iCompany) => {
      this.company = company;
    })
    /*
    router.events.filter((event) => event instanceof NavigationEnd).subscribe(e => {
      const route = e as NavigationEnd;
      this.url = route.url;
    })
    */
  }


  generateTags(tags?: iTags) {
    // default values
    this.tags = {
      title: this.company.name,
      description: this.company.desc,
      image: this.company.logo.original,
      slug: this.url,
      ...tags
    }

    // Set a title
    this.title.setTitle(this.tags.title);

    // Set meta tags
    // this.meta.updateTag({ name: 'twitter:card', content: 'summary' });
    // this.meta.updateTag({ name: 'twitter:site', content: '@angularfirebase' });
    this.meta.updateTag({ name: 'robots', content: "index, follow" })
    this.meta.updateTag({ name: 'description', content: this.company.desc });
    this.meta.updateTag({ name: 'twitter:title', content: this.tags.title });
    this.meta.updateTag({ name: 'twitter:description', content: this.tags.description });
    this.meta.updateTag({ name: 'twitter:image', content: this.tags.image });

    this.meta.updateTag({ property: 'og:type', content: 'article' });
    this.meta.updateTag({ property: 'og:site_name', content: this.company.name });
    this.meta.updateTag({ property: 'og:title', content: this.tags.title });
    this.meta.updateTag({ property: 'og:description', content: this.tags.description });
    this.meta.updateTag({ property: 'og:image', content: this.tags.image });
    this.meta.updateTag({ property: 'og:url', content: `${baseUrl}${this.tags.slug}` });
  }

  get jsonLD() :string {
    /*
    let social = [];
    for(let i in this.company.social){
      social.push(social)
    }
    */
    let json:any = {}/* {
      "@context": "http://schema.org",
      "@type": "Organization",
      name: this.company.name,
      description: this.company.desc,
      logo: this.company.logo.original,
      url: baseUrl,
      email: this.company.mail,
      address: {
             "@type": "PostalAddress",
             streetAddress: this.company.address,
             postalCode: this.company.zipcode,
             addressLocality: this.company.location,
             addressCountry: "NL"
      },
      sameAs:[]
    };*/
    return json;
  }
}
