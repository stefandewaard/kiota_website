import { HomePage } from './home.page';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { SliderComponent } from './slider/slider.component';
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
  imports: [
    ReactiveFormsModule,
    RouterModule.forChild([
      {
        path: '',
        component: HomePage
      }
    ])
  ],
  declarations: [HomePage, SliderComponent]
})
export class HomePageModule {}
