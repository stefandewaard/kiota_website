import { iUser, Roles, READER, EDITOR, ADMIN } from './../models/user.model';
import { Directive, Input, TemplateRef, ViewContainerRef, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Store } from '@ngrx/store';
import * as Reducer from './../reducers';
@Directive({
  selector: '[auth]'
})
export class AuthDirective implements OnInit {

  private $user: Observable<iUser>;
  private roles: Array<Roles> = [READER, EDITOR, ADMIN];
  @Input() auth:Roles;

  constructor(store: Store<Reducer.iState>,
    private templateRef: TemplateRef<any>,
    private viewContainer: ViewContainerRef) {
      this.$user = store.select(Reducer.UserState).map(auth => auth.user);
    }

  ngOnInit() {
    this.$user.subscribe((user: iUser) => {
      this.updateView(user);
    });
  }

  private updateView(user: iUser): void {
    if (this.checkPermission(user)) {
      this.viewContainer.createEmbeddedView(this.templateRef);
    } else {
      this.viewContainer.clear();
    }
  }

  private checkPermission(user: iUser): boolean {
    if (user) {
      let i: number = this.roles.findIndex(role => role === this.auth);
      let u:number = this.roles.findIndex(role => role === user.role);
      if (i <= u) {
        return true;
      } else {
        return false;
      }
    } else { return false; }
  }

}
