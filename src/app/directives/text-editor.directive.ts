import { FireList } from './../../../settings/firebase';
import { Directive, ElementRef, OnInit, OnDestroy } from '@angular/core';
import { NgControl } from "@angular/forms";
import * as firebase from 'firebase'

declare var ClassicEditor;
@Directive({
  selector: '[TextEditor]'
})
export class TextEditorDirective implements OnInit, OnDestroy {
  editor: any;
  constructor(private el: ElementRef, private control: NgControl) { }

  ngOnInit() {
    ClassicEditor.create(this.el.nativeElement, {

    })
      .then(editor => {
        this.editor = editor;
        if (this.control.control.value) {
          editor.setData(this.control.control.value);
        }
        editor.plugins.get('FileRepository').createUploadAdapter = (loader) => {
          const upload = new UploadAdapter(loader);
          return upload;
        };
        editor.model.document.on('change', () => {
          this.control.control.setValue(editor.getData());
        });
      });
  }

  ngOnDestroy() {
    this.editor.destroy()
  }


}

class UploadAdapter {
  loader;
  constructor(loader) {
    this.loader = loader;
  }

  read() {
    return URL.createObjectURL(this.loader.file)
  }

  upload() {
    return new Promise((resolve) => {
      const ref = `${FireList.storage.content}/${this.loader.file.name}`
      firebase.storage().ref(ref).put(this.loader.file)
        .then(() => {
          firebase.storage().ref(ref).getDownloadURL().then((file) => {
            resolve({
              default: file
            })
          })
        })
    })
  }

}
