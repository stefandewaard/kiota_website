import { Action } from '@ngrx/store';
import { DEFAULT_BREAKPOINTS, BreakPoint } from '@angular/flex-layout';

export interface iAppReducer {
  settings: iAppSettings;
  company: iCompany;
  cookies: iCookies;

}
export interface iAppSettings {
  browser: boolean;
  size: size;
  loading: boolean;
  languages:string[];
}
export interface iCompany {
  name: string;
  address: string;
  zipcode: string;
  location: string;
  desc: string;
  logo: {
    header: string,
    footer: string,
    original: string
  };
  mail: string;
  phone: string;
  social: iSocialMedia;
}
interface iSocialMedia {
  facebook?: string;
  instagram?: string;
  twitter?: string;
  linkedin?: string;
  youtube?: string;
  pinterest?: string;
  snapchat?: string
}
export interface iCookies {
  functional: boolean;
  analytical: boolean;
  marketing: boolean;
}

export type size = 'xs' | 'sm' | 'md' | 'lg' | 'xl';



export const initialState: iAppReducer = {
  settings: {
    loading: true,
    browser: false,
    size: null,
    languages:[]
  },
  company: null,
  cookies: {
    functional: true,
    analytical: false,
    marketing: false
  }
}

export const LOADING = "LOADING";
export const NOT_LOADING = "NOT_LOADING";
export const INITIALIZE = "INITIALIZE";
export const SET_SIZE = "SET_SIZE";
export const SET_COOKIES = "SET_COOKIES";

export class Loading implements Action {
  readonly type = LOADING;
  constructor() {

  }
}
export class NotLoading implements Action {
  readonly type = NOT_LOADING;
  constructor() {
  }
}

export class Initialize implements Action {
  readonly type = INITIALIZE;
  payload: {
    browser: boolean,
    company: iCompany,
    languages:string[]
  };
  constructor(payload: {
    browser: boolean,
    company: iCompany,
    languages:string[]
  }) {
    this.payload = payload;
  }
}

export class SetSize implements Action {
  readonly type = SET_SIZE;
  payload: size;
  constructor(width: number) {
    const breakpoints: BreakPoint[] = DEFAULT_BREAKPOINTS.filter((breakpoint) => { return !breakpoint.overlapping })
    let breakpoint: BreakPoint = breakpoints.find(point => { return window.matchMedia(point.mediaQuery).matches })
    this.payload = breakpoint.alias as size;
  }
}

export class SetCookies implements Action {
  readonly type = SET_COOKIES;
  payload: iCookies;
  constructor(payload: any) {
    const cookies: iCookies = {
      functional: typeof payload.functional === "boolean" ? payload.functional : isAccepted(payload.functional),
      analytical: typeof payload.analytical === "boolean" ? payload.analytical : isAccepted(payload.analytical),
      marketing: typeof payload.marketing === "boolean" ? payload.marketing : isAccepted(payload.marketing)
    }
    this.payload = cookies;
  }
}


export type iAccepted = 'accepted' | 'declined' | undefined;

const isAccepted = (value: iAccepted): boolean => {
  return value === 'accepted';
}
export type AppStateActions = Loading | NotLoading | Initialize | SetSize | SetCookies;

export function AppReducer(state = initialState, action: AppStateActions) {
  switch (action.type) {
    case LOADING:
      return { ...state, settings: { ...state.settings, loading: true } };
    case NOT_LOADING:
      return { ...state, settings: { ...state.settings, loading: false } };
    case INITIALIZE:
      return { ...state, company: action.payload.company, settings: { ...state.settings, browser: action.payload.browser, loading: false, languages:action.payload.languages } }
    case SET_SIZE:
      return { ...state, settings: { ...state.settings, size: action.payload } };
    case SET_COOKIES:
      return { ...state, cookies: action.payload };
    default:
      return state;
  }
}


