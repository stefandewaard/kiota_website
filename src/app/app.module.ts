import { config } from './../../settings/firebase';
import { AuthService } from './services/auth.service';
import { RoutingModule } from './modules/routing.module';
import { NgModule, ModuleWithProviders, APP_INITIALIZER } from '@angular/core';
import { LanguageProvider } from './providers/language.provider';
import { NgxJsonLdModule } from '@ngx-lite/json-ld';

import { BrowserModule } from '@angular/platform-browser';

import { TranslateModule, TranslateLoader, MissingTranslationHandler, MissingTranslationHandlerParams } from '@ngx-translate/core';

import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';

import { AngularFireModule } from '@angular/fire';
import {  AngularFireDatabase } from '@angular/fire/database';
import { FirestoreSettingsToken } from '@angular/fire/firestore';


import { AppComponent } from './app.component';
import { HeaderComponent } from './components/header/header.component';
import { FooterComponent } from './components/footer/footer.component';
import { SharedModule } from './modules/shared.module';

import { reducers } from './reducers/index';

export class CustomMissingTranslationHandler implements MissingTranslationHandler {
  handle(params: MissingTranslationHandlerParams) {
    console.warn("Translation missing:", params.key)
    return '--translation missing--';
  }
}

import { AppProvider } from './providers/app.provider';
export const appProvider = (provider: AppProvider) => {
  return () => provider.load();
}

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent
  ],
  imports: [
    BrowserModule.withServerTransition({ appId: config.projectId }),
    EffectsModule.forRoot([AuthService]),
    RoutingModule,
    NgxJsonLdModule,
    StoreModule.forRoot(reducers),
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useClass: LanguageProvider,
        deps: [AngularFireDatabase]
      },
      missingTranslationHandler: { provide: MissingTranslationHandler, useClass: CustomMissingTranslationHandler },
    }),
    AngularFireModule.initializeApp(config),
    SharedModule
  ], providers: [{ provide: APP_INITIALIZER, useFactory: appProvider, deps: [AppProvider], multi: true },
  { provide: FirestoreSettingsToken, useValue: {} }],

  bootstrap: [AppComponent]
})

export class AppModule {
  constructor() {
    //firebase.firestore().settings({ timestampsInSnapshots: true });
  }
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: AppModule
    }
  }
}

