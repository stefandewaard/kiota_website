import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { AngularFireStorageModule } from '@angular/fire/storage';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { AngularFireDatabaseModule } from '@angular/fire/database';
import { FlexLayoutModule } from '@angular/flex-layout';
import { HttpClientModule } from '@angular/common/http';
import { TranslateModule } from '@ngx-translate/core';
import { MaterialModule } from './material.module';

import { TextEditorDirective } from './../directives/text-editor.directive';
import { AuthDirective } from './../directives/auth.directive';

import { SanitizerPipe } from './../pipes/sanitizer.pipe';
import { MomentPipe } from './../pipes/moment.pipe';



@NgModule({
  declarations: [
    AuthDirective,
    SanitizerPipe,
    TextEditorDirective,
    MomentPipe
  ],
  imports: [
    CommonModule,
    AngularFireDatabaseModule,
    AngularFirestoreModule,
    AngularFireAuthModule,
    AngularFireStorageModule,
    FlexLayoutModule,
    FormsModule,
    HttpClientModule,
    MaterialModule,
    ReactiveFormsModule,
    TranslateModule.forChild()
  ],
  exports: [
    /*imports*/
    CommonModule,
    AngularFireDatabaseModule,
    AngularFirestoreModule,
    AngularFireAuthModule,
    AngularFireStorageModule,
    FlexLayoutModule,
    FormsModule,
    HttpClientModule,
    MaterialModule,
    ReactiveFormsModule,
    TranslateModule,
    /*declarations*/
    AuthDirective,
    SanitizerPipe,
    TextEditorDirective,
    MomentPipe,
  ]
})
export class SharedModule { }
