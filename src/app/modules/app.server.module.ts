import { NgModule } from '@angular/core';
import { ServerModule, ServerTransferStateModule } from '@angular/platform-server';
import { FlexLayoutServerModule } from '@angular/flex-layout/server';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { ModuleMapLoaderModule } from '@nguniversal/module-map-ngfactory-loader';

import { AppModule } from '../app.module';
import { AppComponent } from '../app.component';

@NgModule({
  imports: [
    ServerModule,
    ServerTransferStateModule,
    NoopAnimationsModule,
    AppModule.forRoot(),
    FlexLayoutServerModule,
    ModuleMapLoaderModule
  ],
  bootstrap: [AppComponent]
})

export class AppServerModule { }
