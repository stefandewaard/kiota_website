import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import {MatToolbarModule} from '@angular/material';

@NgModule({
  imports: [
    MatToolbarModule
  ],
  exports: [
    MatToolbarModule
],
  providers: [
   // { provide: MAT_SNACK_BAR_DEFAULT_OPTIONS, useValue: { duration: 2500 } }
  ],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA
  ]
})

export class MaterialModule {

}
