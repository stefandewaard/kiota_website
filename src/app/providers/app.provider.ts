import { FireList } from './../../../settings/firebase';
import { Injectable, Inject, PLATFORM_ID } from '@angular/core';
import { AngularFireDatabase } from '@angular/fire/database';
import { isPlatformBrowser } from '@angular/common';
import { Store } from '@ngrx/store';
import * as Reducer from './../reducers';
import * as AppReducer from './../reducers/app.reducer';

@Injectable({
    providedIn: 'root'
})
export class AppProvider {
    constructor(private readonly rtdb: AngularFireDatabase,
        private store: Store<Reducer.iState>,
        @Inject(PLATFORM_ID) private platformId: Object) {
    }
    load(): Promise<boolean> {
        return new Promise((resolve, reject) => {
            this.rtdb.object(FireList.firebase.company).snapshotChanges()
                .subscribe(obj => {
                    const company = obj.payload.val() as AppReducer.iCompany;
                    const browser = isPlatformBrowser(this.platformId);
                    if(browser){
                      console.log(company)
                    }
                    this.rtdb.object(FireList.firebase.language).snapshotChanges().subscribe(lang => {

                        this.store.dispatch(new AppReducer.Initialize({
                            company: company,
                            browser: browser,
                            languages: Object.keys(lang.payload.val())
                        }));
                        resolve(true);
                    });
                });
        });
    }
}
