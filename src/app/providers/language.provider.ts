import { FireList } from './../../../settings/firebase';
import { AngularFireDatabase } from '@angular/fire/database';
import { TranslateLoader } from '@ngx-translate/core';

export class LanguageProvider implements TranslateLoader {

    constructor(private rtdb: AngularFireDatabase) {}

    public getTranslation(lang: string) {
        return this.rtdb.object(`${FireList.firebase.language}/${lang}`).snapshotChanges().map((object) => {
            return object.payload.val();
        })
    }

}
