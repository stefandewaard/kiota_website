import 'zone.js/dist/zone-node';
import 'reflect-metadata';
import * as express from 'express';
import * as functions from 'firebase-functions';


const app = express();

import { join } from 'path';
import { readFileSync } from 'fs';
(global as any).WebSocket = require('ws');
(global as any).XMLHttpRequest = require('xmlhttprequest').XMLHttpRequest;

import { enableProdMode } from '@angular/core';
import { renderModuleFactory } from '@angular/platform-server';
import { provideModuleMap } from '@nguniversal/module-map-ngfactory-loader';

const { AppServerModuleNgFactory, LAZY_MODULE_MAP } = require('./../../dist/server/main');

enableProdMode();

const index = readFileSync(join(__dirname, './../../dist/browser/index2.html'), 'utf8').toString();
let build = join(__dirname, './../../dist/browser');

app.use(express.static(build));

app.get('**', (req, res) => {
  renderModuleFactory(AppServerModuleNgFactory, {
    url: req.path,
    document: index,
    extraProviders: [
      provideModuleMap(LAZY_MODULE_MAP)
    ]
  }).then(html => res.status(200).send(html));
});

app.set('view engine', 'html');

export let server = functions.https.onRequest(app);
