import * as path from 'path';
import * as fs from 'fs';
import * as os from 'os';
import * as sizeOf from 'image-size';
const gcs = require('@google-cloud/storage')();
const spawn = require('child-process-promise').spawn;
import * as functions from 'firebase-functions';
import * as isImage from 'is-image';


export let resizeImage = functions.storage.object().onFinalize((object, context) => {
    const filePath: string = path.dirname(object.name);
    const ext:string = path.extname(object.name);




    let width: number;
    console.log(object.metadata, ext)
    if (isImage(object.name) && !object.metadata['resize']) {
        switch (filePath) {
            case 'prijzen':
                width = 200;
                return ResizeImage(object, width)/*.then((check) => {
                    console.log(check);
                });*/
            case 'content':
                width = 300;
                return ResizeImage(object, width)/*.then((check) => {
                    console.log(check);
                });*/
            case 'slider':
                width = 600;
                return ResizeImage(object, width)/*.then((check) => {
                    console.log(check);
                });*/
            default:
                width = 300;
                return ResizeImage(object, width)/*.then((check) => {
                    console.log(check);
                });*/
        }
    } else {
        return false;
    }
});

const ResizeImage = async (object, width: number) => {
    const contentType = object.contentType; // File content type.
    const fileBucket = object.bucket; // The Storage bucket that contains the file.
    const filePath = object.name; // File path in the bucket.
    const fileName = path.basename(filePath);
    const bucket = gcs.bucket(fileBucket);
    const tempFilePath = path.join(os.tmpdir(), fileName);
    const metadata = { contentType: contentType };
    const thumbFilePath = path.join(path.dirname(object.name), fileName);
    //Download
    await bucket.file(object.name).download({ destination: tempFilePath })
    //Resize
    const dimensions = sizeOf(tempFilePath);
    if (dimensions.width > width) {
        await spawn('convert', [tempFilePath, '-resize', resize(tempFilePath, width), tempFilePath]);
    }
    // Upload
    await bucket.upload(tempFilePath, { destination: thumbFilePath, metadata: { ...metadata, resize: 'done' } });
    fs.unlinkSync(tempFilePath);
    return true;
}

const resize = (data, width) => {
    const dimensions = sizeOf(data);
    let height;
    if (dimensions.width < width) {
        let i = width / dimensions.width;
        height = dimensions.height * i;
    } else {
        let i = dimensions.width / width;
        height = dimensions.height / i;
    }
    return `${width}x${height}`;
}
